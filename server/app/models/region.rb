class Region < ActiveRecord::Base
	attr_accessible :name
	self.table_name = 'regions'
	self.primary_key = 'name'

	has_many :points, :foreign_key => 'name', :primary_key => 'region'
end
