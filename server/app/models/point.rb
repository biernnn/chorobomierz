class Point < ActiveRecord::Base
		self.table_name = 'points'
		self.primary_key = 'point_id'

		belongs_to :region, :foreign_key => 'region'
	attr_accessible :point_id, :geo, :region
end
