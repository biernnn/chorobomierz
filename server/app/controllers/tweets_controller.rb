class TweetsController < ApplicationController

	def index
		@tweets = Tweet.all

		respond_to do |format|
			format.json { render :json => @tweets }
		end
	end

end
