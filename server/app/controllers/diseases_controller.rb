class DiseasesController < ApplicationController

	def index
		@diseases = Disease.all

		respond_to do |format|
			format.json { render :json => @diseases }
		end
	end

end
