class ApiActionsController < ApplicationController

  include ApiActionsHelper

  def fetch_data
	logger.debug "Fetching data for start_date:" + params[:start_date] + ", end_date:" + params[:end_date] + " region:" + params[:region_name] + ", disease:" + params[:disease_name]
	json_result = generate_json_for_client(params[:start_date], params[:end_date], params[:region_name], params[:disease_name]) 
	
	respond_to do |format|
		format.json { render :json => json_result }
	end
  end
end
