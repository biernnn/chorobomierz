module ApiActionsHelper

	def generate_json_for_client(start_date, end_date, region_name, disease_name) 
		results = prepare_results(start_date, end_date, region_name, disease_name)
		result_grouped_by_dates = Hash.new{|hash, key| hash[key] = Array.new;}
		results.each_entry do |row| 
			if result_grouped_by_dates.has_key?(row['tweet_date']).nil?
			   result_grouped_by_dates[row['tweet_date']] = []
			end
			   result_grouped_by_dates[row['tweet_date']].push(row)
			logger.debug "Data fetched: " + row.to_json
		end
		result_grouped_by_dates
	end
	
	def prepare_results(start_date, end_date, region_name, disease_name) 
		connection = ActiveRecord::Base.connection
		prepare_query = "SELECT prepare_data_for_client('" + start_date + "','" + end_date + "','" + region_name + "','" + disease_name + "')"
		connection.execute(prepare_query)
		results = connection.execute("SELECT * FROM client_select_result_holder")
		logger.debug "Deleting temporary data from"
		connection.execute("DELETE FROM client_select_result_holder")
		logger.debug "Returning result set"
		results
	end
end
