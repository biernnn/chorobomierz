ActiveAdmin.register Point do
  
  form do |f|                         
    f.inputs "Point" do       
      f.input :geo                  
      f.input :region                  
    end                               
    f.buttons                         
  end                                 

  controller do
	  def create
		  point = Point.create(:geo => params[:point][:geo], :region => Region.find_by_name(params[:point][:region]))
		  redirect_to :action => :index
	  end
  end
end
