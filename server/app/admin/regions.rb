ActiveAdmin.register Region do

  form do |f|                         
    f.inputs "Region" do       
      f.input :name                  
    end                               
    f.buttons                         
  end                                 
  
end
