class CreatePrepareDataForClientFunction < ActiveRecord::Migration
  def up
	  execute 'CREATE OR REPLACE FUNCTION prepare_data_for_client(start_date date, end_date date, region_name character varying, disease_name character varying)
					 RETURNS void AS
					 $BODY$
						DECLARE
							record_result RECORD;
						BEGIN
							FOR record_result IN SELECT 
									t.tweet_date as tweet_date, 
									t.geo as geo, 
									get_amount_of_disease_occurrences(t.tweet_date, r.name, d.name, p.geo) as all_occurreces_during_day,
									get_disease_intensity(t.tweet_date, r.name, d.name, p.geo) as disease_intensity
								FROM 
									tweets t
								JOIN 
									diseases d ON d.disease_id = t.disease_id
								JOIN 
									points p ON p.point_id = t.point_id
								JOIN 
									regions r ON r.name = p.region
								WHERE
									d.name = disease_name AND r.name = region_name AND
									(t.tweet_date >= start_date AND t.tweet_date < end_date)
								GROUP BY 
									t.tweet_date, t.geo, 3, 4 
							LOOP
								INSERT INTO client_select_result_holder(tweet_date, geo, all_occurreces_during_day, disease_intensity)  values (record_result.tweet_date, record_result.geo, record_result.all_occurreces_during_day, record_result.disease_intensity);
							END LOOP;
							RETURN;
						END;
						$BODY$
						LANGUAGE plpgsql VOLATILE
						COST 100;'
  end

  def down
			execute 'DROP FUNCTION prepare_data_for_client(date, date, character varying, character varying);'
  end
end
