class CreateGetDiseaseIntensityFunction < ActiveRecord::Migration
  def up
	execute 'CREATE OR REPLACE FUNCTION get_disease_intensity(input_tweet_date date, region_name character varying, disease_name character varying, point_geo character varying)
			 RETURNS double precision AS
			$BODY$
				DECLARE
					occurrences_within_region integer;
					single_point_occurrences integer;
				BEGIN
					occurrences_within_region := (SELECT
						COUNT(*) 
					FROM 
						tweets t 
					JOIN 
						diseases d ON d.disease_id = t.disease_id 
					JOIN 
						points p ON p.point_id = t.point_id 
					WHERE 
						p.region = region_name AND d.name = disease_name AND t.tweet_date = input_tweet_date
					GROUP BY 
						t.tweet_date);

					single_point_occurrences := get_amount_of_disease_occurrences(input_tweet_date, region_name, disease_name, point_geo);
					
					return single_point_occurrences::float / occurrences_within_region;
				END;
				$BODY$
			  LANGUAGE plpgsql IMMUTABLE SECURITY DEFINER
			  COST 10;'
  end

  def down
	execute 'DROP FUNCTION get_disease_intensity(date, character varying, character varying, character varying);'
  end
end
