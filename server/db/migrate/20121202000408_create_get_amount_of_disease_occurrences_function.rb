class CreateGetAmountOfDiseaseOccurrencesFunction < ActiveRecord::Migration
  def up
	execute 'CREATE OR REPLACE FUNCTION get_amount_of_disease_occurrences(input_tweet_date date, region_name character varying, disease_name character varying, point_geo character varying)
			 RETURNS integer AS
			 $BODY$
				DECLARE
					occurrences integer;
				BEGIN
					occurrences := (SELECT
						COUNT(*) 
					FROM 
						tweets t 
					JOIN 
						diseases d ON d.disease_id = t.disease_id 
					JOIN 
						points p ON p.point_id = t.point_id 
					WHERE 
						p.region = region_name AND d.name = disease_name AND t.tweet_date = input_tweet_date AND point_geo = p.geo
					GROUP BY 
						t.tweet_date);

					return occurrences;
				END;
				$BODY$
			  LANGUAGE plpgsql IMMUTABLE SECURITY DEFINER
			  COST 10;'
  end

  def down
	execute 'DROP FUNCTION get_amount_of_disease_occurrences(date, character varying, character varying, character varying);'
  end
end
