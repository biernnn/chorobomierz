class CreateTweets < ActiveRecord::Migration

  	def up
		execute 'CREATE TABLE IF NOT EXISTS tweets (
			tweet_id VARCHAR(65) NOT NULL,
			disease_id INTEGER NOT NULL,
			point_id INTEGER NOT NULL,
			tweet_date DATE NOT NULL,
			query VARCHAR(255) NOT NULL,
			geo VARCHAR(127) NOT NULL,
			PRIMARY KEY (tweet_id, disease_id, point_id),
			FOREIGN KEY (disease_id) REFERENCES diseases (disease_id),
			FOREIGN KEY (point_id) REFERENCES points (point_id)
		);'
	end

	def down
		execute 'DROP TABLE tweets'
	end

end
