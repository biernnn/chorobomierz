class CreateClientSelectResultHolder < ActiveRecord::Migration
  def up
	execute 'CREATE TABLE client_select_result_holder
			(
			  tweet_date date,
			  geo character varying(127),
			  all_occurreces_during_day integer,
			  disease_intensity double precision
			)'
  end

  def down
	execute 'DROP TABLE client_select_result_holder;'
  end
end
