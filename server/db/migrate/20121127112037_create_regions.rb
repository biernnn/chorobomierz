class CreateRegions < ActiveRecord::Migration
	def up
		execute 'CREATE TABLE IF NOT EXISTS regions (
			name VARCHAR(65) NOT NULL,
			PRIMARY KEY(name)
		);
		'
	end

	def down
		execute 'DROP TABLE regions'
	end
end