class CreatePoints < ActiveRecord::Migration
  def up
	execute 'CREATE TABLE IF NOT EXISTS points (
		point_id SERIAL,
		geo VARCHAR(127) NOT NULL,
		region VARCHAR(65) NOT NULL,
		PRIMARY KEY (point_id),
		FOREIGN KEY (region) REFERENCES regions (name)
	);'
  end

  def down
	  execute 'DROP TABLE points'
  end
end
