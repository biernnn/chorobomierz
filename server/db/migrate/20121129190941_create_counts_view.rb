class CreateCountsView < ActiveRecord::Migration
  def up
	execute 'CREATE VIEW counts AS (
		SELECT 
			diseases.disease_id, points.point_id, tweets.tweet_date, COUNT(tweets.tweet_id) 
		FROM
			diseases
		LEFT JOIN
			tweets on diseases.disease_id = tweets.disease_id
		JOIN
			points on points.point_id = tweets.point_id
		GROUP BY points.point_id, diseases.disease_id, tweets.tweet_date
	);'
  end

  def down
	  execute 'DROP VIEW counts'
  end
end
