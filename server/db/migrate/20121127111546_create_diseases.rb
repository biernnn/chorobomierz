class CreateDiseases < ActiveRecord::Migration
  def self.up
	execute 'CREATE TABLE IF NOT EXISTS diseases (
		disease_id SERIAL UNIQUE,
		name VARCHAR(255),
		query VARCHAR(255)
	);'
  end

  def self.down
	execute 'DROP TABLE diseases'
  end
end
