class CreateMaxIdsView < ActiveRecord::Migration
  def up
	  execute 'CREATE VIEW max_ids AS (
		SELECT 
			diseases.disease_id, points.point_id, MAX(tweets.tweet_id) as max_id
		FROM
			diseases
		LEFT JOIN
			tweets ON diseases.disease_id=tweets.disease_id
		JOIN
			points ON points.point_id = tweets.point_id
		GROUP BY points.point_id, diseases.disease_id
	);'
  end

  def down
	  execute 'DROP VIEW max_ids'
  end
end
