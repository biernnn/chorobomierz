# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121203224935) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "client_select_result_holder", :id => false, :force => true do |t|
    t.date    "tweet_date"
    t.string  "geo",                       :limit => 127
    t.integer "all_occurreces_during_day"
    t.float   "disease_intensity"
  end

  create_table "diseases", :id => false, :force => true do |t|
    t.integer "disease_id",               :null => false
    t.string  "name",       :limit => 10
    t.string  "query"
  end

  add_index "diseases", ["disease_id"], :name => "diseases_disease_id_key", :unique => true

  create_table "points", :primary_key => "point_id", :force => true do |t|
    t.string "geo",    :limit => 127, :null => false
    t.string "region", :limit => 65,  :null => false
  end

  create_table "regions", :id => false, :force => true do |t|
    t.string "name", :limit => 65, :null => false
  end

  create_table "tweets", :id => false, :force => true do |t|
    t.string  "tweet_id",   :limit => 65,  :null => false
    t.integer "disease_id",                :null => false
    t.integer "point_id",                  :null => false
    t.date    "tweet_date",                :null => false
    t.string  "query",                     :null => false
    t.string  "geo",        :limit => 127, :null => false
  end

end
