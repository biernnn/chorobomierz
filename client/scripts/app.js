$(function () {
    $('#map-controls-pan').click(function () {
        MAP.fit();
    });

    $('#map-controls-dump').click(function () {
        console.log(JSON.stringify(MAP.dumpData(), undefined, 4));
    });

    MAP.edit = true;
    MAP.initialize();

     MAP.clearData();
//     var currentDate = new Date();
//     MAP.setData(MAP_DATA[currentDate.format("yyyy-mm-dd")]);

//    MAP.randomizer(MAP_SAMPLE_POS);
//    MAP.fit();
});
