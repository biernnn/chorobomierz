
function CircleLabel (circle, label) {
    this._circle = circle;
    this.labelDiv_ = document.createElement("div");
    this.labelDiv_.style.cssText = "position: absolute; overflow: hidden;";

    this.eventDiv_ = document.createElement("div");
    this.eventDiv_.style.cssText = this.labelDiv_.style.cssText;
}


CircleLabel.prototype = new google.maps.OverlayView();


CircleLabel.prototype.onAdd = function () {
  var me = this;
  var cMouseIsDown = false;
  var cDraggingInProgress = false;
  var cSavedPosition;
  var cSavedZIndex;
  var cLatOffset, cLngOffset;
  var cIgnoreClick;

  // Stops all processing of an event.
  //
  var cAbortEvent = function (e) {
    if (e.preventDefault) {
      e.preventDefault();
    }
    e.cancelBubble = true;
    if (e.stopPropagation) {
      e.stopPropagation();
    }
  };

  this.getPanes().overlayImage.appendChild(this.labelDiv_);
  this.getPanes().overlayMouseTarget.appendChild(this.eventDiv_);

  this.listeners_ = [
    // google.maps.event.addDomListener(document, "mouseup", function (mEvent) {
    //   if (cDraggingInProgress) {
    //     mEvent.latLng = cSavedPosition;
    //     cIgnoreClick = true; // Set flag to ignore the click event reported after a label drag
    //     google.maps.event.trigger(me._circle, "dragend", mEvent);
    //   }
    //   cMouseIsDown = false;
    //   google.maps.event.trigger(me._circle, "mouseup", mEvent);
    // }),
    // google.maps.event.addListener(me._circle.getMap(), "mousemove", function (mEvent) {
    //   if (cMouseIsDown && me._circle.getDraggable()) {
    //     // Change the reported location from the mouse position to the marker position:
    //     mEvent.latLng = new google.maps.LatLng(mEvent.latLng.lat() - cLatOffset, mEvent.latLng.lng() - cLngOffset);
    //     cSavedPosition = mEvent.latLng;
    //     if (cDraggingInProgress) {
    //       google.maps.event.trigger(me._circle, "drag", mEvent);
    //     } else {
    //       // Calculate offsets from the click point to the marker position:
    //       cLatOffset = mEvent.latLng.lat() - me._circle.getPosition().lat();
    //       cLngOffset = mEvent.latLng.lng() - me._circle.getPosition().lng();
    //       google.maps.event.trigger(me._circle, "dragstart", mEvent);
    //     }
    //   }
    // }),

    // google.maps.event.addDomListener(this.eventDiv_, "mouseover", function (e) {
    //   me.eventDiv_.style.cursor = "pointer";
    //   google.maps.event.trigger(me._circle, "mouseover", e);
    // }),

    // google.maps.event.addDomListener(this.eventDiv_, "mouseout", function (e) {
    //   me.eventDiv_.style.cursor = me._circle.getCursor();
    //   google.maps.event.trigger(me._circle, "mouseout", e);
    // }),

    // google.maps.event.addDomListener(this.eventDiv_, "click", function (e) {
    //   if (cIgnoreClick) { // Ignore the click reported when a label drag ends
    //     cIgnoreClick = false;
    //   } else {
    //     cAbortEvent(e); // Prevent click from being passed on to map
    //     google.maps.event.trigger(me._circle, "click", e);
    //   }
    // }),

    // google.maps.event.addDomListener(this.eventDiv_, "dblclick", function (e) {
    //   cAbortEvent(e); // Prevent map zoom when double-clicking on a label
    //   google.maps.event.trigger(me._circle, "dblclick", e);
    // }),

    // google.maps.event.addDomListener(this.eventDiv_, "mousedown", function (e) {
    //   cMouseIsDown = true;
    //   cDraggingInProgress = false;
    //   cLatOffset = 0;
    //   cLngOffset = 0;
    //   cAbortEvent(e); // Prevent map pan when starting a drag on a label
    //   google.maps.event.trigger(me._circle, "mousedown", e);
    // }),

    // google.maps.event.addListener(this._circle, "dragstart", function (mEvent) {
    //   cDraggingInProgress = true;
    //   cSavedZIndex = me._circle.getZIndex();
    // }),

    // google.maps.event.addListener(this._circle, "drag", function (mEvent) {
    //   me._circle.setPosition(mEvent.latLng);
    //   // me._circle.setZIndex(1000000); // Moves the marker to the foreground during a dr

    // }),
    // google.maps.event.addListener(this._circle, "dragend", function (mEvent) {
    //   cDraggingInProgress = false;
    //   me._circle.setZIndex(cSavedZIndex);
    // }),
    google.maps.event.addListener(this._circle, "position_changed", function () {
        console.log('position changed');
        me.setPosition();
    }),
    google.maps.event.addListener(this._circle, "zindex_changed", function () {
      me.setZIndex();
    }),
    google.maps.event.addListener(this._circle, "visible_changed", function () {
      me.setVisible();
    }),
    google.maps.event.addListener(this._circle, "labelvisible_changed", function () {
      me.setVisible();
    }),
    google.maps.event.addListener(this._circle, "title_changed", function () {
      me.setTitle();
    }),
    google.maps.event.addListener(this._circle, "labelcontent_changed", function () {
      me.setContent();
    }),
    google.maps.event.addListener(this._circle, "labelanchor_changed", function () {
      me.setAnchor();
    }),
    google.maps.event.addListener(this._circle, "labelclass_changed", function () {
      me.setStyles();
    }),
    google.maps.event.addListener(this._circle, "labelstyle_changed", function () {
      me.setStyles();
    })
  ];
};

CircleLabel.prototype.draw = function () {
    this.setContent();
    this.setTitle();
    this.setStyles();
};


/**
 * Sets the content of the label.
 * The content can be plain text or an HTML DOM node.
 * @private
 */
CircleLabel.prototype.setContent = function () {
    var content = this._circle.get("labelContent");
    if (typeof content.nodeType === "undefined") {
        this.labelDiv_.innerHTML = content;
        this.eventDiv_.innerHTML = this.labelDiv_.innerHTML;
    } else {
        this.labelDiv_.appendChild(content);
        content = content.cloneNode(true);
        this.eventDiv_.appendChild(content);
    }
};

/**
 * Sets the content of the tool tip for the label. It is
 * always set to be the same as for the marker itself.
 * @private
 */
CircleLabel.prototype.setTitle = function () {
    this.eventDiv_.title = this._circle.getTitle() || "";
};

/**
 * Sets the style of the label by setting the style sheet and applying
 * other specific styles requested.
 * @private
 */
CircleLabel.prototype.setStyles = function () {
  var i, labelStyle;

  // Apply style values from the style sheet defined in the labelClass parameter:
  this.labelDiv_.className = this._circle.get("labelClass");
  this.eventDiv_.className = this.labelDiv_.className;

  // Clear existing inline style values:
  this.labelDiv_.style.cssText = "";
  this.eventDiv_.style.cssText = "";
  // Apply style values defined in the labelStyle parameter:
  labelStyle = this._circle.get("labelStyle");
  for (i in labelStyle) {
    if (labelStyle.hasOwnProperty(i)) {
      this.labelDiv_.style[i] = labelStyle[i];
      this.eventDiv_.style[i] = labelStyle[i];
    }
  }
  this.setMandatoryStyles();
};

/**
 * Sets the mandatory styles to the DIV representing the label as well as to the
 * associated event DIV. This includes setting the DIV position, zIndex, and visibility.
 * @private
 */
CircleLabel.prototype.setMandatoryStyles = function () {
  this.labelDiv_.style.position = "absolute";
  this.labelDiv_.style.overflow = "hidden";
  // Make sure the opacity setting causes the desired effect on MSIE:
  if (typeof this.labelDiv_.style.opacity !== "undefined") {
    this.labelDiv_.style.filter = "alpha(opacity=" + (this.labelDiv_.style.opacity * 100) + ")";
  }

  this.eventDiv_.style.position = this.labelDiv_.style.position;
  this.eventDiv_.style.overflow = this.labelDiv_.style.overflow;
  this.eventDiv_.style.opacity = 0.01; // Don't use 0; DIV won't be clickable on MSIE
  this.eventDiv_.style.filter = "alpha(opacity=1)"; // For MSIE

  this.setAnchor();
  this.setPosition(); // This also updates zIndex, if necessary.
  this.setVisible();
};

/**
 * Sets the anchor point of the label.
 * @private
 */
CircleLabel.prototype.setAnchor = function () {
  var anchor = this._circle.get("labelAnchor");
  this.labelDiv_.style.marginLeft = -anchor.x + "px";
  this.labelDiv_.style.marginTop = -anchor.y + "px";
  this.eventDiv_.style.marginLeft = -anchor.x + "px";
  this.eventDiv_.style.marginTop = -anchor.y + "px";
};

/**
 * Sets the position of the label. The zIndex is also updated, if necessary.
 * @private
 */
CircleLabel.prototype.setPosition = function () {
  var position = this.getProjection().fromLatLngToDivPixel(this._circle.getCenter());

  this.labelDiv_.style.left = position.x + "px";
  this.labelDiv_.style.top = position.y + "px";
  this.eventDiv_.style.left = this.labelDiv_.style.left;
  this.eventDiv_.style.top = this.labelDiv_.style.top;

  this.setZIndex();
};

CircleLabel.prototype.setZIndex = function () {
    var zAdjust = (this._circle.get("labelInBackground") ? -1 : +1);
    this.labelDiv_.style.zIndex = 100000; //parseInt(this.labelDiv_.style.top, 10) + zAdjust;
    this.eventDiv_.style.zIndex = this.labelDiv_.style.zIndex + 1000000;
};

/**
 * Sets the visibility of the label. The label is visible only if the marker itself is
 * visible (i.e., its visible property is true) and the labelVisible property is true.
 * @private
 */
CircleLabel.prototype.setVisible = function () {
    if (this._circle.get("labelVisible")) {
        this.labelDiv_.style.display = this._circle.getVisible() ? "block" : "none";
    } else {
        this.labelDiv_.style.display = "none";
    }
    this.eventDiv_.style.display = this.labelDiv_.style.display;
};

CircleLabel.prototype.onRemove = function () {
  var i;
  this.labelDiv_.parentNode.removeChild(this.labelDiv_);
  this.eventDiv_.parentNode.removeChild(this.eventDiv_);

  // Remove event listeners:
  for (i = 0; i < this.listeners_.length; i++) {
    google.maps.event.removeListener(this.listeners_[i]);
  }
};
