function renderChart (containerId, titleText, subtitleText, inputData, xAxisCategories) {
    var chart;
    chart = new Highcharts.Chart({
        chart: {
            renderTo: containerId,
            type: 'line',
            marginRight: 130,
            marginBottom: 60
        },
        title: {
            text: titleText,
            x: -20 //center
        },
        subtitle: {
            text: subtitleText,
            x: -20
        },
        xAxis: {
            categories: xAxisCategories,
            labels: {
                rotation: -45,
                align: 'right',
                style: {
                    fontSize: '10px',
                    fontFamily: 'arial'
                }
            }
        },
        yAxis: {
            title: {
                text: 'Wystąpienia choroby'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                this.x +': '+ this.y ;
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        series: inputData,
        plotOptions: {
            series: {
                cursor: 'pointer',
                events: {
                    click: function(event) {
                        var count = event.point.y;
                        var date = event.point.category;
                        var disease = event.point.series.name;
                        $("#date").datepicker('setDate', date);
                        $("#diseaseSelect").val(disease);
                        fillMapInfoViaAjax();
                    }
                }
            }
        }
    });
  
}
