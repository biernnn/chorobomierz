var HOST = "http://chorobomierz.herokuapp.com";
var DISEASES_HOST_ACTION = "/diseases.json";
var REGIONS_HOST_ACTION = "/regions.json";
var FETCH_DATA_ACTION = "/fetch_data.json";
var HOST_CHART_DATA_TYPE = "statistics_data"
var HOST_MAP_DATA_TYPE = "map_data"

//$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

$(function(){
    $("#diseaseSelect, #regionSelect").change(function(){
        fillMapInfoViaAjax();
    });
});

$(function(){
    $("#regionSelect").change(function(){
        fillChartsViaAjax();
    });
});


function fillMapInfoViaAjax() {
    $.blockUI();
    var regionName = $("#regionSelect").val();
    var diseaseName = $("#diseaseSelect").val();
    // asynchronous workaround
    if (!regionName){
        regionName = "USA";
    }
    if (!diseaseName) {
        diseaseName = "tuberculosis";
    }
    var endDate = $('#date').datepicker('getDate', '+1d'); 
    endDate.setDate(endDate.getDate()+1); 
    $.ajaxq("mainAjaxQueue", {
        url: HOST + FETCH_DATA_ACTION,
        dataType:"json",
        timeout : 100000,
        data: {
            start_date : $('#date').datepicker('getDate').format("yyyy-mm-dd"),
            end_date : endDate.format("yyyy-mm-dd"),
            region_name : regionName,
            disease_name : diseaseName,
            data_type : HOST_MAP_DATA_TYPE
        },
        success: function(data){
            MAP.clearData();
            MAP_DATA = adaptExternalJson(data);
//            MAP_DATA = data;
            MAP.setData(MAP_DATA[$("#date").datepicker('getDate').format("yyyy-mm-dd")]);
        },
        complete : function(){
            $.unblockUI();
        }
    });
}

//browser killer - adrian dzieki za zmiane api w jsonie
function adaptExternalJson(data) {
    var newArray = [];
    for (var element in data){
        newArray[element] = [];
        for(var i=0;i<element.length;i++){
            newArray[element][i] = {};
            newArray[element][i]['intensity'] = data[element][i]['disease_intensity'];
            newArray[element][i]['count'] = data[element][i]['all_occurreces_during_day'];
            newArray[element][i]['geo'] = data[element][i]['geo'];
        }
    }
    return newArray;
}

function fillRegionsViaAjax(select) {
    genericFillerViaAjaxCall(REGIONS_HOST_ACTION, select)
}

function fillDiseasesViaAjax(select) {
    genericFillerViaAjaxCall(DISEASES_HOST_ACTION, select)
}

function genericFillerViaAjaxCall (hostAction, select) {
    $.ajaxq("mainAjaxQueue", {
        url : HOST + hostAction,
        dataType: "json",
        timeout : 100000,
        success: function(data){
            if (data){
                $.each(data, function(){
                    select.append($("<option />").val(this.name).text(this.name));
                });
            }
        }
    });
}

function adaptChartData(data) {
    var series = getDiseasesSeriesStructure();
    for (var date in data) {
        for (var j = 0; j< series.length; j++) {
            var count = 0;
            for (var i = 0 ; i< data[date].length; i++) {
                if (data[date][i].hasOwnProperty(series[j].name)) {
                    count = parseInt(data[date][i][series[j].name],10);
                    break;
                }
            }
            series[j].data.push(count);
        }
    }
    return series;
}

function getDiseasesSeriesStructure() {
    var series = [];
    $.each($("#diseaseSelect").children(), function() {
        var disease = {name:$(this).val(), data: []};
        series.push (disease);
    });
    return series;
};

function fillChartsViaAjax() {
    fillWeekChart();
    fillMonthChart();   
}

function fillWeekChart(){
    var startDate = $('#date').datepicker('getDate', '-1w'); 
    startDate.setDate(startDate.getDate()-7); 
    startDate = startDate.format("yyyy-mm-dd");
    var endDate = $('#date').datepicker('getDate').format("yyyy-mm-dd");
    var regionName = $("#regionSelect").val();
    // asynchronous workaround
    if (!regionName){
        regionName = "USA";
    }
    $.ajaxq("mainAjaxQueue", {
        url : HOST + FETCH_DATA_ACTION,
        dataType : "json",
//        async : false,
        cache : false,
        timeout: 100000,
        data : {
            data_type : HOST_CHART_DATA_TYPE,
            start_date : startDate,
            end_date : endDate,
            region_name : regionName
        },
        success: function (data) {
           var series = adaptChartData(data);
           var xAxisCategories = _.keys(data);
           renderChart("weekChart", "Tydzień zachorowań", "źródło: epidemiologist, tweeter", series, xAxisCategories);
        }
    });
    
}

function fillMonthChart(){
    var startDate = $('#date').datepicker('getDate', '-1m'); 
    startDate.setDate(startDate.getDate()-30); 
    startDate = startDate.format("yyyy-mm-dd");
    var endDate = $('#date').datepicker('getDate').format("yyyy-mm-dd");
    var regionName = $("#regionSelect").val();
    // asynchronous workaround
    if (!regionName){
        regionName = "USA";
    }
    $.ajaxq("mainAjaxQueue", {
        url : HOST + FETCH_DATA_ACTION,
        dataType : "json",
//        async : false,
        cache : false,
        timeout: 100000,
        data : {
            data_type : HOST_CHART_DATA_TYPE,
            start_date : startDate,
            end_date : endDate,
            region_name : regionName
        },
        success: function (data) {
           var series = adaptChartData(data);
           var xAxisCategories = _.keys(data);
           renderChart("monthChart", "Miesiąc zachorowań", "źródło: epidemiologist, tweeter", series, xAxisCategories);
        }
    });    
}

function prepareXCategories (data) {
    var newDates = [];
    for (var i = 0; i< data.length; i++){
        if (data[i].substr(-2, 2) === "01") {
            newDates[i] = data[i]
        } else {
            newDates[i] = "";
        }
    }
    return newDates;
}