function Circle (data) {
    var edit = data.type == 'edit',
        that = this;

    this.id = data.geo;

    data.labelAnchor = new google.maps.Point(25, 14);
    data.labelClass = "labels";
    data.labelStyle = {  };
    data.labelVisible = true;
    data.intensity = data.intensity === undefined ? 0 : data.intensity;

    this.set('intensity', data.intensity);
    this.set('edit', edit);

    if (edit) {
        data.editable = true;
        data.draggable = true;
        data.clickable = true;
    } else {

        data.labelStyle.color = data.strokeColor = this.calcColor(data.intensity);
        data.strokeOpacity = 0.8;
        data.strokeWeight = 1;
        data.fillColor = '#7285FF';
        data.fillOpacity = 0.15;

        this.label = new CircleLabel(this);
    }

    google.maps.Circle.apply(this, [data]);

    this.setMap(MAP.map);

    google.maps.event.addListener(this, 'rightclick', function (e) {
        if (that.get('edit')) {
            that.destroy();
        }
    });

    google.maps.event.addListener(this, 'intensity_changed', function (e) {
        that.setColors();
    });

    // TODO: To trzeba robić poza klasą 'Circle'
    MAP.circles.push(this);
};

Circle.prototype = new google.maps.Circle();


Circle.prototype.seriesDump = function () {
    return {
        label: this.get('labelContent'),
        geo: this.getGeoString()
    };
};

Circle.prototype.seriesUpdate = function (series) {
    this.set('labelContent', series.count);
    this.set('intensity', series.intensity);
};

Circle.prototype.getTitle = function () {
    return this.getGeoString();
};

Circle.prototype.getGeoString = function () {
    var center = this.getCenter();

    return center.lat() + ',' + center.lng() + ',' +
        this.getRadius() / 1000 + 'km';
};

Circle.prototype.setMap = function (theMap) {
    google.maps.Circle.prototype.setMap.apply(this, arguments);

    if (this.label) {
        this.label.setMap(theMap);
    }
};

Circle.prototype.setColors = function () {
    var style = this.get('labelStyle');

    style.color = this.calcColor(this.get('intensity'));
    this.set('strokeColor', style.color);
    this.set('labelStyle', style);
};

Circle.prototype.destroy = function () {
    this.setMap(null);
};

Circle.prototype.calcColor = function (intensity) {
    var r = parseInt(55 + 200 * intensity),
        g = parseInt(200 * (1 - intensity)),
        b = 0;

    return 'rgb(' + r + ',' + g +',' + b + ')';
};

window.MAP = MAP = {};

MAP.defaultRadius = 50 * 1000;

MAP.circles = [];


MAP.setState = function () {

};

MAP.editMode = function () {

};

MAP.initialize = function () {

    var mapOptions = {
        center: new google.maps.LatLng(37.6335, -98.6658),
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        disableDoubleClickZoom: true
    };

    var map = MAP.map = new google.maps.Map(
        document.getElementById('map_canvas'), mapOptions);

    google.maps.event.addListener(map, 'dblclick', function (e) {
        if (MAP.edit) {
            var c = new Circle({
                center: e.latLng,
                radius: MAP.defaultRadius,
                type: 'edit'
            });
        }
    });
};

MAP.circleFromSeries = function (series) {
    var data = {},
        coords = series.geo.split(','),
        radius = coords[2];

    if (radius.indexOf('km') >= 0) {
        radius = parseInt(radius) * 1000;
    } else {
        radius = parseInt(radius);
    }

    return new Circle ({
        id: series.geo,
        map: MAP.map,
        center: new google.maps.LatLng(
            parseInt(coords[0]), parseInt(coords[1])),
        radius: radius,
        labelContent: series.count || 0,
        intensity: series.intensity,
        edit: data.edit
    });
};


MAP.dumpData = function () {
    var res = [];

    MAP.circles.forEach(function (c) {
        if (c.get('edit')) {
            res.push(c.seriesDump());
        }
    });

    return res;
};


MAP.setData = function (series) {
    if (series) {
    series.forEach(function (s) {
        for (var i = 0; i < MAP.circles.length; i++) {
            if (s.geo == MAP.circles[i].id) {
                MAP.circles[i].seriesUpdate(s);
                return;
            }
        }
        MAP.circleFromSeries(s);
    });
    } else {
        $.unblockUI();
        console.log('received empty data');
    }
};

MAP.clearData = function () {
    for (var i = 0; i < MAP.circles.length; i++) {
        MAP.circles[i].destroy();
    }
    MAP.circles = [];
};

MAP.fit = function () {
    var bounds = MAP.circles[0].getBounds();

    MAP.circles.slice(1).forEach(function (c) {
        bounds = bounds.union(c.getBounds());
    });

    MAP.map.fitBounds(bounds);
};

MAP.randomizer = function (series) {
    setInterval(function () {
        series.forEach(function (s) {
            s.count = parseInt(Math.random() * 100 + 100);
            s.intensity = (s.count - 100) / 100;
        });
        MAP.setData(series);
    }, 1000);
};
