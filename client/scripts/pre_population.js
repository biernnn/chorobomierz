$(document).ready(function(){
    fillRegionsViaAjax($("#regionSelect"));
    fillDiseasesViaAjax($("#diseaseSelect"));
    fillMapInfoViaAjax();
    fillChartsViaAjax();
});
