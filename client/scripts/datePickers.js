function handleNewDate(newDate) {
    MAP.clearData();
    $("#date").datepicker("setDate", newDate);
    if (MAP_DATA[newDate.format("yyyy-mm-dd")]){        
        MAP.setData(MAP_DATA[newDate.format("yyyy-mm-dd")]);
    } else {
        fillMapInfoViaAjax();
    }
}

$(function() {
        $( "#date" ).datepicker({
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: "yy-mm-dd",
            maxDate : "+0d",
//            defaultDate : "2012-12-03",
            firstDay : 1,
            onSelect : function(date){
                fillMapInfoViaAjax();
                MAP.clearData();
                if (MAP_DATA[date]){
                    MAP.setData(MAP_DATA[date]);
                }
            }
        });
//        $("#date").datepicker('setDate', new Date());
        $("#date").datepicker('setDate', "2012-12-03");
        
        $("#map-controls-prev-day").click(function(){
            var newDate = $('#date').datepicker('getDate', '-1d'); 
            newDate.setDate(newDate.getDate()-1); 
            handleNewDate(newDate);
        });
        
        $("#map-controls-next-day").click(function(){
            var newDate = $('#date').datepicker('getDate', '+1d'); 
            newDate.setDate(newDate.getDate()+1); 
            handleNewDate(newDate);
        });
    });