CREATE TABLE IF NOT EXISTS diseases (
	disease_id INT(5) NOT NULL AUTO_INCREMENT,
	name VARCHAR(31),
	query VARCHAR(255),
	PRIMARY KEY (disease_id)
);


CREATE TABLE IF NOT EXISTS regions (
	nazwa VARCHAR(65) NOT NULL,
	PRIMARY KEY(nazwa)
);


CREATE TABLE IF NOT EXISTS points (
	point_id int(10) NOT NULL AUTO_INCREMENT,
	geo VARCHAR(127) NOT NULL,
	region VARCHAR(65) NOT NULL,
	PRIMARY KEY (point_id),
	FOREIGN KEY (region) REFERENCES regions (nazwa)
);

CREATE TABLE IF NOT EXISTS tweets (
	tweet_id VARCHAR(65) NOT NULL,
	disease_id INT(5) NOT NULL,
	point_id INT(10) NOT NULL,
	tweet_date DATE NOT NULL,
	tweet_text TEXT,
	query VARCHAR(255) NOT NULL,
	geo VARCHAR(127) NOT NULL,
	PRIMARY KEY (tweet_id, disease_id,point_id),
	FOREIGN KEY (disease_id) REFERENCES diseases (disease_id),
	FOREIGN KEY (point_id) REFERENCES points (point_id)
);

CREATE TABLE IF NOT EXISTS last_ids (
	disease_id INT(5) NOT NULL,
	point_id INT(10) NOT NULL,
	last_id INT(10) NOT NULL,
	PRIMARY KEY (disease_id, point_id),
	FOREIGN KEY (disease_id) REFERENCES diseases (disease_id),
	FOREIGN KEY (point_id) REFERENCES points (point_id)
);

CREATE VIEW max_ids AS (
	SELECT 
	diseases.disease_id AS disease_id, 
	points.point_id AS point_id,
	CASE WHEN MAX(tweets.tweet_id) IS NULL THEN 0 ELSE MAX(tweets.tweet_id) END AS max_id
	FROM
	diseases
	JOIN
	points
	LEFT JOIN
	tweets
	ON points.point_id=tweets.point_id AND diseases.disease_id=tweets.disease_id
	GROUP BY points.point_id, diseases.disease_id
);

CREATE VIEW counts AS (
	SELECT 
		diseases.disease_id AS disease_id, 
		points.point_id AS point_id,
		tweets.tweet_date AS tweet_date,
		CASE WHEN COUNT(tweets.tweet_id) IS NULL THEN 0 ELSE COUNT(tweets.tweet_id) END AS count
	FROM
		diseases
	JOIN
		points
	LEFT JOIN
		tweets
	ON 
	points.point_id=tweets.point_id AND diseases.disease_id=tweets.disease_id
	GROUP BY points.point_id, diseases.disease_id,tweets.tweet_date
);


